# ioBroker.apagerpro

Alamos app for mobile alterting. The aPagerPro adapter for ioBroker has not been released.

| Option                   | Description                                  |
|--------------------------|----------------------------------------------|
| `Client ID`              | User specific aPagerHub client ID.           |
| `Expire alarm [seconds]` | Set alarm state to `null` after this period. |
