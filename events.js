'use strict';

const EventSource = require('eventsource');

const StateMap = {
  [EventSource.CONNECTING]: 'Connecting',
  [EventSource.OPEN]: 'Open',
  [EventSource.CLOSED]: 'Closed',
};

let connected = false;

const source = new EventSource('http://apagerhub.drizzd.de:8080/events/foo');
source.addEventListener('connected', () => {
  console.log('Connected event.');
});
source.addEventListener('heartbeat', () => {
  console.log('Heartbeat received.');
});
source.onopen = (_) => {
  console.log('Connection established.');
  connected = true;
};
source.onmessage = (e) => {
  console.log('Message:', e.data);
};
source.onerror = (err) => {
  connected = source.readyState === EventSource.OPEN;
  if (source.readyState === EventSource.CLOSED && err.message === undefined) {
    console.error('Connection closed.');
    return;
  }
  if (source.readyState === EventSource.CONNECTING && err.message === undefined) {
    console.error('Connection lost.');
    return;
  }
  console.error('Error:', err.message);
  //source.close();
};
