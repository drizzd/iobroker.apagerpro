'use strict';

/*
 * Created with @iobroker/create-adapter v2.5.0
 */

// The adapter-core module gives you access to the core ioBroker functions
// you need to create an adapter
const utils = require('@iobroker/adapter-core');
const EventSource = require('eventsource');

// Load your modules here, e.g.:
// const fs = require("fs");

class Apagerpro extends utils.Adapter {
    /**
     * @param {Partial<utils.AdapterOptions>} [options={}]
     */
    constructor(options) {
        super({
            ...options,
            name: 'apagerpro',
        });
        this.source = null;
        this.on('ready', this.onReady.bind(this));
        this.on('unload', this.onUnload.bind(this));
    }

    /**
     * Is called when databases are connected and adapter received configuration.
     */
    async onReady() {
        this.log.info('config clientId: ' + this.config.clientId);

        await this.setObjectNotExistsAsync('info.connection', {
            type: 'state',
            common: {
                name: 'info.connection',
                type: 'boolean',
                role: 'indicator',
                read: true,
                write: false,
            },
            native: {},
        });
        await this.setObjectNotExistsAsync('alarm', {
            type: 'state',
            common: {
                name: 'alarm',
                type: 'boolean',
                role: 'sensor.alarm',
                read: true,
                write: false,
            },
            native: {},
        });

        this.connectHub();
    }

    connectHub() {
        if (!this.validClientId()) {
            this.log.info(`Invalid client ID: ${this.config.clientId}`);
            return;
        }

        this.source = new EventSource(`http://apagerhub.drizzd.de:8080/events/${this.config.clientId}`);
        this.source.onopen = (_) => {
            this.setState('info.connection', { val: true, ack: true });
            this.log.info(`Connection established.`);
        };
        this.source.onmessage = (e) => {
            this.setState('alarm', { val: true, ack: true, expire: this.config.expireSeconds ?? 10 });
            this.log.info(`apagerhub: ${JSON.stringify(e.data)}`);
        };
        this.source.onerror = (err) => {
            this.setState('info.connection', { val: this.source.readyState === EventSource.OPEN, ack: true });
            if (err.message === undefined && this.source.readyState !== EventSource.OPEN) {
                this.log.error(`Connection lost.`);
            }
            if (err.message !== undefined) {
                this.log.error(`apagerhub: Error: ${JSON.stringify(err)}`);
            }
        };
    }

    validClientId() {
      return this.config.clientId !== undefined && this.config.clientId.match(/^[0-9A-Za-z_-]+$/);
    }

    /**
     * Is called when adapter shuts down - callback has to be called under any circumstances!
     * @param {() => void} callback
     */
    onUnload(callback) {
        try {
            // Here you must clear all timeouts or intervals that may still be active
            // clearTimeout(timeout1);
            // clearTimeout(timeout2);
            // ...
            // clearInterval(interval1);
            if (this.source !== null) {
                this.log.info('Closing connection.');
                this.source.close();
            }
            this.setState('info.connection', { val: false, ack: true });

            callback();
        } catch (e) {
            callback();
        }
    }
}

if (require.main !== module) {
    // Export the constructor in compact mode
    /**
     * @param {Partial<utils.AdapterOptions>} [options={}]
     */
    module.exports = (options) => new Apagerpro(options);
} else {
    // otherwise start the instance directly
    new Apagerpro();
}
